/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bce.ddi.behavior;

import bce.ddi.entity.Persona;

/**
 *
 * @author Byron Bonilla
 */
public class PersonaEdad implements PredicadoPersona {

    @Override
    public boolean filtrar(Persona persona) {
        return persona.getEdad() >= 18;
    }
    
}
