/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bce.ddi.entity;

/**
 *
 * @author Byron Bonilla
 */
public class Persona implements Comparable<Persona>{
    
    private Integer perCodigo;
    private String perNombre;
    private String perApellido;
    private char genero;
    private Integer edad;

    @Override
    public String toString() {
        return "Persona{" + "perCodigo=" + perCodigo + ", perNombre=" + perNombre + ", perApellido=" + perApellido + ", genero=" + genero + ", edad=" + edad + '}';
    }
    

    public Persona(int perCodigo, String perNombre, String perApellido, char genero, int edad) {
        this.perCodigo = perCodigo;
        this.perNombre = perNombre;
        this.perApellido = perApellido;
        this.genero = genero;
        this.edad = edad;
    }
    
    /**
     * @return the perCodigo
     */
    public Integer getPerCodigo() {
        return perCodigo;
    }

    /**
     * @param perCodigo the perCodigo to set
     */
    public void setPerCodigo(Integer perCodigo) {
        this.perCodigo = perCodigo;
    }

    /**
     * @return the perNombre
     */
    public String getPerNombre() {
        return perNombre;
    }

    /**
     * @param perNombre the perNombre to set
     */
    public void setPerNombre(String perNombre) {
        this.perNombre = perNombre;
    }

    /**
     * @return the perApellido
     */
    public String getPerApellido() {
        return perApellido;
    }

    /**
     * @param perApellido the perApellido to set
     */
    public void setPerApellido(String perApellido) {
        this.perApellido = perApellido;
    }

    /**
     * @return the genero
     */
    public char getGenero() {
        return genero;
    }

    /**
     * @param genero the genero to set
     */
    public void setGenero(char genero) {
        this.genero = genero;
    }

    /**
     * @return the edad
     */
    public Integer getEdad() {
        return edad;
    }

    /**
     * @param edad the edad to set
     */
    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    @Override
    public int compareTo(Persona t) {
        return getPerCodigo().compareTo(t.getPerCodigo());        
    }
    
    
}
