/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bce.ddi.test;

import bce.ddi.entity.Persona;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author Byron Bonilla
 * @author Roberto Lagos
 */
public class TestOrdenamiento {

    /**
     * @param args main
     */
    public static void main(String[] args) {        
        List<Persona> lstPersona = Arrays.asList(
                new Persona(0, "Byron", "Bonilla", 'M', 34),
                new Persona(1, "Juan", "Estrella", 'M', 25),
                new Persona(2, "Martha", "Andrade", 'F', 17),
                new Persona(3, "Maria", "Garcia", 'F', 20),
                new Persona(4, "Carlos", "Ortiz", 'M', 17)
        );
        
        
        System.out.println("Aplicando Ordenamientos");
        System.out.println("=======================");
        
        System.out.println("Ordenamiento Natural");
        System.out.println("--------------------");
        lstPersona.forEach(System.out::println);
        
        System.out.println("\nOrdenamiento Descendente Clase Anónima");
        System.out.println("--------------------------------------");        
        Comparator<Persona> comparadorCodigo = new Comparator<Persona>(){
            @Override
			public int compare(Persona p1, Persona p2) {
				return p2.getPerCodigo().compareTo(p1.getPerCodigo());
			}
        };
        
        Collections.sort(lstPersona, comparadorCodigo);
        lstPersona.forEach(System.out::println);
        
        System.out.println("\nOrdenamiento Ascendente Edad Lambda");
        System.out.println("-------------------------------------");        
        Comparator<Persona> comparadorEdad = (Persona p1, Persona p2) -> p1.getEdad().compareTo(p2.getEdad());
        Collections.sort(lstPersona,comparadorEdad);
        lstPersona.forEach(System.out::println);
        
        

		
        
    }
    
}
