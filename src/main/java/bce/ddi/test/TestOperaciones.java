/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bce.ddi.test;

import bce.ddi.entity.Persona;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import static java.util.Comparator.comparing;
import java.util.List;

/**
 *
 * @author Byron Bonilla
 * @author Roberto Lagos
 */
public class TestOperaciones {

    /**
     * @param args main
     */
    public static void main(String[] args) {        
        List<Persona> lstPersona = Arrays.asList(
                new Persona(0, "Byron", "Bonilla", 'M', 34),
                new Persona(1, "Juan", "Estrella", 'M', 25),
                new Persona(2, "Martha", "Andrade", 'F', 17),
                new Persona(3, "Maria", "Garcia", 'F', 20),
                new Persona(4, "Carlos", "Ortiz", 'M', 17)
        );
        
        System.out.println("Operaciones utilizando Streams");
        System.out.println("==============================");
                
        System.out.println("\nObtener los tres primeros elementos");
        System.out.println("--------------------------------------");
        lstPersona.stream().limit(3).forEach(System.out::println);
        
        System.out.println("\nSaltarse los tres primeros elementos");
        System.out.println("--------------------------------------");
        lstPersona.stream().skip(3).forEach(System.out::println);
        
        System.out.println("\nElementos ordenados Comparable");
        System.out.println("--------------------------------");
        lstPersona.stream().sorted().forEach(System.out::println);
        
        System.out.println("\nElementos ordenados Comparator");
        System.out.println("--------------------------------");
        Collections.sort(lstPersona, new Comparator<Persona>(){
            @Override
            public int compare(Persona o1, Persona o2) {
		return o1.getPerNombre().compareTo(o2.getPerNombre());
            }
        });
        lstPersona.forEach(System.out::println);
        
        System.out.println("\nElementos ordenados por nombre al revés");
        System.out.println("-----------------------------------------");
        lstPersona.stream().sorted(comparing(Persona::getPerNombre).reversed()).forEach(System.out::println);
        
        
        
        System.out.println("\nFiltrado utilizando Streams");
        System.out.println("===========================");
        
        System.out.println("\nPersonas Mayores de Edad");
        System.out.println("--------------------------");
        lstPersona.stream().filter((Persona p) -> p.getEdad() >= 18).forEach(System.out::println);
        
        System.out.println("\nPersonas Genero Masculino y Mayores de Edad Lambda");
        System.out.println("----------------------------------------------------");        
        lstPersona.stream().filter((Persona p) -> p.getGenero() == 'M').filter((Persona p) -> p.getEdad() >= 18).forEach(System.out::println);
        
        
    }
    
}
