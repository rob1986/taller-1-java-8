/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bce.ddi.test;

import bce.ddi.controlador.ControladorPersona;
import bce.ddi.entity.Persona;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;


/**
 *
 * @author Byron Bonilla
 * @author Roberto Lagos
 */
public class TestPredicate {

    /**
     * @param args main
     */
    public static void main(String[] args) {       
        List<Persona> lstPersona = Arrays.asList(
                new Persona(0, "Byron", "Bonilla", 'M', 34),
                new Persona(1, "Juan", "Estrella", 'M', 25),
                new Persona(2, "Martha", "Andrade", 'F', 17),
                new Persona(3, "Maria", "Garcia", 'F', 20),
                new Persona(4, "Carlos", "Ortiz", 'M', 17)
        );
        
        System.out.println("Aplicando Predicate");
        System.out.println("===================");
        
        System.out.println("\nPersonas Menores de Edad");
        System.out.println("--------------------------");
        Predicate<Persona> predicadoEdad = (Persona p) -> p.getEdad() < 18;
        List<Persona> lstFiltradoEdad = ControladorPersona.filtrar(lstPersona, predicadoEdad);
        lstFiltradoEdad.forEach(System.out::println);
               
        System.out.println("\nPersonas Genero Femenino");
        System.out.println("----------------------------");
        Predicate<Persona> predicadoGenero = (Persona p) -> p.getGenero() == 'F';
        List<Persona> lstFiltradoGenero = ControladorPersona.filtrar(lstPersona, predicadoGenero);
        lstFiltradoGenero.forEach(System.out::println);
       
        System.out.println("\nPersonas Genero Femenino y Menores de Edad");
        System.out.println("----------------------------------------------------");
        Predicate<Persona> predicadoGeneroEdad = (Persona p) -> p.getGenero() == 'F' && p.getEdad() < 18;
        List<Persona> lstFiltradoGeneroEdad = ControladorPersona.filtrar(lstPersona, predicadoGeneroEdad);
        lstFiltradoGeneroEdad.forEach(System.out::println);
    }
    
}
