/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bce.ddi.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 *
 * @author Byron Bonilla
 * @author Roberto Lagos
 */
public class TestRendimiento {

    public static void generarArreglo(List<Integer> lstNumeros, Integer maximo) {
        Random rnd = new Random();
        for (int i = 0; i < maximo; i++) {
            lstNumeros.add(rnd.nextInt(100));
        }
    }

    public static List<Integer> procesarLista(List<Integer> lstNumeros, boolean modo) {
        List<Integer> listaOrdenada;
        if (modo) {
            listaOrdenada = lstNumeros.stream().sorted().collect(Collectors.toList());
        } else {
            listaOrdenada = lstNumeros.parallelStream().sorted().collect(Collectors.toList());
        }
        return listaOrdenada;
    }

    /**
     * @param args main
     */
    public static void main(String[] args) {
        Integer numeroRegistros = 100000;        
        List<Integer> lstNumeros = new ArrayList<>();
        List<Integer> lstOrdenada;
        generarArreglo(lstNumeros, numeroRegistros);
        System.out.println("Lista : " + lstNumeros);

        long tiempoInicialStream = System.currentTimeMillis();
        lstOrdenada = procesarLista(lstNumeros, true);
        long tiempoFinalStream = System.currentTimeMillis() - tiempoInicialStream;
        System.out.println("Lista ordenada: " + lstOrdenada);

        long tiempoInicialParallelStream = System.currentTimeMillis();
        lstOrdenada = procesarLista(lstNumeros, false);
        long tiempoFinalParallelStream = System.currentTimeMillis() - tiempoInicialParallelStream;
        System.out.println("Lista ordenada: " + lstOrdenada);
        
        System.out.println(String.format("Tiempo de duración proceso de ordenamiento con Stream y %d registros: %d ms", numeroRegistros, tiempoFinalStream));
        System.out.println(String.format("Tiempo de duración proceso de ordenamiento con ParallelStream y %d registros: %d ms", numeroRegistros, tiempoFinalParallelStream));

    }

}
