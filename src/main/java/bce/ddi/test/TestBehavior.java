/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bce.ddi.test;

import bce.ddi.behavior.PersonaEdad;
import bce.ddi.behavior.PersonaGenero;
import bce.ddi.controlador.ControladorPersona;
import bce.ddi.entity.Persona;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Byron Bonilla
 * @author Roberto Lagos
 */
public class TestBehavior {

    /**
     * @param args main
     */
    public static void main(String[] args) {                
        List<Persona> lstPersona = Arrays.asList(
                new Persona(0, "Byron", "Bonilla", 'M', 34),
                new Persona(1, "Juan", "Estrella", 'M', 25),
                new Persona(2, "Martha", "Andrade", 'F', 17),
                new Persona(3, "Maria", "Garcia", 'F', 20),
                new Persona(4, "Carlos", "Ortiz", 'M', 17)
        );
        
        System.out.println("Filtrado utilizando Behavior Parametrization");
        System.out.println("============================================");
        
        System.out.println("\nPersonas Mayores de Edad");
        System.out.println("--------------------------");
        List<Persona> lstFiltradoEdad = ControladorPersona.filtrar(lstPersona, new PersonaEdad());
        lstFiltradoEdad.forEach(System.out::println);
        
        System.out.println("\nPersonas Genero Masculino");
        System.out.println("----------------------------");
        List<Persona> lstFiltradoGenero = ControladorPersona.filtrar(lstPersona, new PersonaGenero());
        lstFiltradoGenero.forEach(System.out::println);
        
        System.out.println("\nPersonas Genero Masculino y Mayores de Edad Lambda");
        System.out.println("----------------------------------------------------");
        List<Persona> lstFiltradoGeneroEdad = ControladorPersona.filtrar(lstPersona, (Persona p) -> (p.getGenero() == 'M' && p.getEdad() >= 18));
        lstFiltradoGeneroEdad.forEach(System.out::println);
    }
    
}
