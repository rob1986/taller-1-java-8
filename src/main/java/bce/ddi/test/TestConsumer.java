/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bce.ddi.test;

import bce.ddi.controlador.ControladorPersona;
import bce.ddi.entity.Persona;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Byron Bonilla
 * @author Roberto Lagos
 */
public class TestConsumer {

    /**
     * @param args main
     */
    public static void main(String[] args) {        
        List<Persona> lstPersona = Arrays.asList(
                new Persona(0, "Byron", "Bonilla", 'M', 34),
                new Persona(1, "Juan", "Estrella", 'M', 25),
                new Persona(2, "Martha", "Andrade", 'F', 17),
                new Persona(3, "Maria", "Garcia", 'F', 20),
                new Persona(4, "Carlos", "Ortiz", 'M', 17)
        );
        
        System.out.println("Aplicando Consumer");
        System.out.println("==================");
        System.out.println("\nNombres en Mayuscula");
        System.out.println("--------------------");
        ControladorPersona.ejecutar(lstPersona, (Persona p) -> System.out.println(p.getPerNombre().toUpperCase() + " " + p.getPerApellido().toUpperCase()));
        
                
    }
}
