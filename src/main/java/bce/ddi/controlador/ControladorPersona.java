/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bce.ddi.controlador;

import bce.ddi.behavior.PredicadoPersona;
import bce.ddi.entity.Persona;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 *
 * @author Byron Bonilla
 */
public class ControladorPersona {

    //Behavior
    public static List<Persona> filtrar(List<Persona> lstPersona, PredicadoPersona predicado) {
        List<Persona> lstResultado = new ArrayList<>();
        for (Persona persona : lstPersona) {
            if (predicado.filtrar(persona)) {
                lstResultado.add(persona);
            }
        }
        return lstResultado;
    }

    //Predicate
    public static <T> List<Persona> filtrar(List<Persona> lstPersona, Predicate<T> predicate) {
        List<Persona> lstResultados = new ArrayList<>();
        for (Persona persona : lstPersona) {
            if (predicate.test((T) persona)) {
                lstResultados.add(persona);
            }
        }
        return lstResultados;
    }

    public static <T> void ejecutar(List<Persona> lstPersona, Consumer<T> consumer) {
        for (Persona persona : lstPersona) {
            consumer.accept((T) persona);
        }
    }

}
